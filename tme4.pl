%de  toldi, de bezenac
%T-BOX
subs(chat, felin).
subs(lion, felin).
subs(chien,canide).
subs(canide,chien).
subs(chihuahua,chien).
subs(souris, mammifere).
subs(felin, mammifere).
subs(canide, mammifere).
subs(animal, etreVivant).
subs(mammifere, animal).
subs(canari,  animal).
subs(and(mammifere,plante), nothing).
subs( and(animal, some(aMaitre)),pet).
subs(pet, some(aMaitre)).
subs(some(aMaitre), all(aMaitre, humain)).
subs(chihuahua, and(chien, pet)).

carnivoreExc(X) :- mange(X,animal),not(mange(X,plante)).


carnivoreExc(lion).
carnivoreExc(X):-predateur(X).

herbivoreExc(X) :- mange(X,plante),not(mange(X,animal)).

mange(X,_):-animal(X).

chat(Felix).
humain(Pierre).
aMaitre(Felix,Pierre).

chihuahua(Princesse).
humain(Marie).
aMaitre(Princesse,Marie).

souris(Jerry).
canari(Titi).

mange(Felix,Titi).
mange(Felix,Jerry).

subS1(C,D):-subs(C,D);C==D.
subS1(C,D):-subs(C,X),subS1(X,D).

subsS(C,C,_).
subsS(C,D,[]):-subs(C,D).
subsS(C,D,L):-subs(C,E),not(member(E,L)),append(L,[E],X),subsS(E,D,X).

subsS(C, D):-subsS(C, D, []).