% DE TOLDI, DE BEZENAC

% ------------- T-BOX ------------------
subs(chat, felin).
subs(lion, felin).
subs(chien,canide).
subs(canide,chien).
subs(chihuahua,chien).
subs(souris, mammifere).
subs(felin, mammifere).
subs(canide, mammifere).
subs(animal, etreVivant).
subs(mammifere, animal).
subs(canari,  animal).
subs(and(mammifere,plante), nothing).
subs( and(animal, some(aMaitre)),pet).
subs(pet, some(aMaitre)).
subs(some(aMaitre), all(aMaitre, humain)).
subs(chihuahua, and(chien, pet)).

subs(C,and(X,Y)):-subs(C,X),subs(C,Y).


% carnivoreExc mange uniquement des animaux
subs(carnivoreExc, all(mange, animal)).
% tout ce qui mange uniquement des animaux sont des carnivoresExc
subs(all(mange,animal), carnivoreExc).

% herbivoreExc mange uniquement des plantes
subs(herbivoreExc, all(mange, plante)).
% tout ce qui mange uniquement des plantes est un herbivore
subs(all(mange, plante), herbivoreExc).

subs(lion, carnivoreExc).
subs(carnivoreExc, predateur).

% pour tout animal, il existe quelque chose tel que lanimal le mange
subs(animal, some(mange)).

% on ne peut pas manger rien et manger quelque chose
subs(and(all(mange,nothing),some(mange)),nothing).

% --------------------------------------
%trouver toutes les solutions: findall(Z, f(Z, ...), Bag).


% ------------- Exercice2 --------------
subS1(C,D):-subs(C,D);C==D.
subS1(C,D):-subs(C,X),subS1(X,D).

subsS(C,C,_).
subsS(C,D,_):-subs(C,D).
subsS(C,D,L):-subs(C,E),not(member(E,L)),append(L,[E],Xsubs),subsS(E,D,X).


subsS(C, D):-subsS(C, D, []).
% --------------------------------------


% ------------- Exercice3 --------------
% 1. ----pas sur de la reponse---


subsInter(C, D1, D2, _):-subs(C, D1),subs(C, D2);subs(C,and(D1,D2));subs(C,and(D2,D1)).
subsInter(C, D1, D2, L):-subsS(C,X),not(member(X,L)),append(L,[X],Z),subsInter(C,X,D2,Z);subsS(C,Y),not(member(Y,M)),append(M,[Y],Z),subsInter(C,D1,Y,Z).

%subsInter1_1(C, D1, D2):-subsS(C, and(D1,D2));subsS(C,and(D2,D1)).

%subsInter1(C, D1, D2):-subsS(C, D1), subsS(C, D2) ; subsS(C, and(D1,D2)) ;subsS(C,and(D2,D1)). 

% 2. ----pas sur de la reponse---
% subsInter2_1(C1, C2, D):-subsS(and(C1, C2), D).

subsInter2(C1, C2, D):-subsS(C1, D),subsS(C2, D) ; subsS(and(C1, C2), D).
% 3. Il faudrait aussi vérifier si lintersection de concepts atomiques est subsummée par une autre intersection

subsInter_3(C1, C1, D1, D2):-subsInter2_1(C1,C2,D1), subsInter2_1(C1,C2,D2).

% 4. 


% --------------------------------------

% ------------- A-BOX ------------------
inst(felix, chat).
inst(pierre, homme).
inst(princesse, chihuahua).
inst(marie, humain).
inst(jerry, souris).
inst(titi, canari).

instR(felix, aMaitre, pierre).
instR(princesse, aMaitre, chihuahua).
instR(felix, mange, jerry).
instR(felix, mange, titi).
% --------------------------------------

% carnivoreExc(X) :- mange(X,animal),not(mange(X,plante)).
% carnivotreExc(X):-not( mange(X, not(animal)).
% carnivoreExc(lion).
% carnivoreExc(X):-predateur(X).
% herbivoreExc(X) :- mange(X,plante),not(mange(X,animal)).
% mange(X,_):-animal(X).
% chat(Felix).
% humain(Pierre).
% aMaitre(Felix,Pierre).
% chihuahua(Princesse).
% humain(Marie).
% aMaitre(Princesse,Marie).
% souris(Jerry).
% canari(Titi).
% mange(Felix,Titi).
% mange(Felix,Jerry).
